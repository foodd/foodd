stage = "dev"
environment="development"

#-----
# VPC
#-----

cidr = "10.1.0.0/16"
private_subnets = [
  "10.1.1.0/24",
  "10.1.2.0/24",
  "10.1.3.0/24"]

database_subnets = [
  "10.1.51.0/24",
  "10.1.52.0/24",
  "10.1.53.0/24"]

public_subnets = [
  "10.1.101.0/24",
  "10.1.102.0/24",
  "10.1.103.0/24"]

## DNS
enable_dns_hostnames = "true"
enable_dns_support = "true"

assign_generated_ipv6_cidr_block = "true"

## One NAT Gateway per subnet
enable_nat_gateway = "true"
single_nat_gateway = "false"
one_nat_gateway_per_az = "false"


#----
# EC2
#----

instance_type = "t3.medium"