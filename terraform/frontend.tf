module "frontend_label" {
  source  = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.4.0"

  namespace  = var.namespace
  name       = var.name
  stage      = var.stage
  delimiter  = var.delimiter
  attributes = ["frontend"]
  tags       = merge(var.tags, map("DailyShutdown", "true"), map("DailyStartup", "true")  )
}

resource "aws_security_group" "fronted" {
  name_prefix = "${module.frontend_label.id}${var.delimiter}"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    # You should allow only CIDR blocks that you trust
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.frontend_label.tags
}

resource "aws_security_group" "frontend_alb" {
  name_prefix = "${module.frontend_label.id}${var.delimiter}"
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port = 0
    protocol = "tcp"
    to_port = 0

    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.frontend_label.tags
}

module "frontend_key_pair" {
  source = "git::https://github.com/cloudposse/terraform-aws-key-pair.git?ref=tags/0.4.0"

  namespace             = var.namespace
  stage                 = var.stage
  name                  = "${var.name}${var.delimiter}frontend"
  ssh_public_key_path   = "${path.module}/secrets"
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
  chmod_command         = "chmod 600 %v"
}

resource "aws_lb" "frontend" {
  name               = module.frontend_label.id
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.frontend_alb.id]
  subnets            = module.vpc.public_subnets

  enable_deletion_protection = true

  tags = module.frontend_label.tags
}

resource "aws_instance" "frontend" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type

  vpc_security_group_ids = [aws_security_group.fronted.id]
  subnet_id              = module.vpc.public_subnets[0]
  key_name               = module.frontend_key_pair.key_name

  credit_specification {
    cpu_credits = "standard"
  }

  tags        = module.frontend_label.tags
  volume_tags = module.frontend_label.tags

  depends_on = ["aws_security_group.fronted"]
}