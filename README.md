

![logo][logo]





 
# terranix 


 
Automate your AWS environment with Terraform and LinuxAcademy. 


---


## Screenshots


![demo](docs/screenshots/demo.gif) |
|:--:|
| *How to initialize project using [roots][roots]* |









## Requirements

In order to run this project you will need: 

- [Ubuntu][ubuntu] - Ubuntu is a complete Linux operating system, freely available with both community and professional support.
- [Ansible][ansible] - Ansible is the simplest way to automate apps and IT infrastructure.
- [Terraform][terraform] - Write, Plan, and Create Infrastructure as Code
- [Packer][packer] - Build Automated Machine Images
- [Docker][docker] - Securely build, share and run any application, anywhere




## Usage

1) Fork this project
2) Start an AWS Sandbox via Linux Academy
3) Copy and paste the newly AWS Access Key ID and Secret Key into the directory `aws/credentials`

```
# To initialize the project
make init

# Possible values for environment can be: development, testing, staging, production
make terraform/install terraform/plan ENVIRONMENT=development

```



## Quick start

Here's how to get started: 

- `make init` - to initialize the [roots][root]




## Examples

Here are some real world examples: 

- [terraform-null-label](https://github.com/cloudposse/terraform-null-label/) - A terraform module that leverages `terraform/%` targets




## Makefile targets

```Available targets:

  ansible/requirements               	Install Roles from file requirements.yml
  aws-nuke/install                   	Install aws-nuke
  base                               	Runs base playbook
  clean                              	Clean roots
  code/install                       	Install Visual Studio Code
  docker/build                       	Build docker image
  docker/clean                       	Cleanup docker
  docker/clean/containers            	Cleanup docker containers
  docker/clean/images/all            	Cleanup docker images all
  docker/clean/images                	Cleanup docker images
  docker/clean/networks              	Cleanup docker networks
  docker/clean/volumes               	Cleanup docker volumes
  docker/image/promote/local         	Promote $SOURCE_DOCKER_REGISTRY/$IMAGE_NAME:$SOURCE_VERSION to $TARGET_DOCKER_REGISTRY/$IMAGE_NAME:$TARGET_VERSION
  docker/image/promote/remote        	Pull $SOURCE_DOCKER_REGISTRY/$IMAGE_NAME:$SOURCE_VERSION and promote to $TARGET_DOCKER_REGISTRY/$IMAGE_NAME:$TARGET_VERSION
  docker/image/push                  	Push $TARGET_DOCKER_REGISTRY/$IMAGE_NAME:$TARGET_VERSION
  docker/inspect                     	Return low-level information on $(DOCKER_IMAGE_NAME)
  docker/install                     	Install docker
  docker/list                        	List all docker containers and images
  docker/stop/container              	Stop the docker container $(DOCKER_CONTAINER_NAME)
  dropbox/install                    	Install Dropbox
  gomplate/install                   	Install Gomplate
  gomplate/version                   	Displays Gomplate version
  google-chrome/install              	Install google-chrome
  help/all                           	Display help for all targets
  help/all/plain                     	Display help for all targets
  help                               	Help screen
  help/short                         	This help short screen
  java/install                       	Install java
  openvpn/install                    	Install openvpn
  packer/install                     	Install packer
  packer/version                     	Prints the packer version
  peek/install                       	Install peek
  peek/version                       	Prints the peek version
  pip/install                        	Install pip
  readme                             	Alias for readme/build
  readme/build                       	Create README.md by building it from README.yaml
  readme/install                     	Install README
  security/install                   	Install security
  spotify/install                    	Install spotify
  terraform/apply                    	Builds or changes infrastructure
  terraform/clean                    	Cleans Terraform vendor from Maker
  terraform/console                  	Interactive console for Terraform interpolations
  terraform/destroy                  	Destroy Terraform-managed infrastructure, removes .terraform and local state files
  terraform/fmt                      	Rewrites config files to canonical format
  terraform/get                      	Download and install modules for the configuration
  terraform/graph                    	Create a visual graph of Terraform resources
  terraform/init                     	Initialize a Terraform working directory
  terraform/init-s3-backend          	Initialize a Terraform working directory with S3 as backend and DynamoDB for locking
  terraform/install                  	Install terraform
  terraform/output                   	Read an output from a state file
  terraform/plan                     	Generate and show an execution plan
  terraform/providers                	Prints a tree of the providers used in the configuration
  terraform/push                     	Upload this Terraform module to Atlas to run
  terraform/refresh                  	Update local state file against real resources
  terraform/show                     	Inspect Terraform state or plan
  terraform/taint                    	Manually mark a resource for recreation
  terraform/untaint                  	Manually unmark a resource as tainted
  terraform/validate                 	Validates the Terraform files
  terraform/version                  	Prints the Terraform version
  terraform/workspace                	Select workspace
  update                             	Updates roots
  vagrant/destroy                    	Stops and deletes all traces of the vagrant machine
  vagrant/install                    	Install vagrant
  vagrant/recreate                   	Destroy and creates the vagrant environment
  vagrant/update/boxes               	Updates all Vagrant boxes
  vagrant/up                         	Starts and provisions the vagrant environment
  version                            	Displays versions of many vendors installed
  virtualbox/install                 	Install virtualbox
  vlc/install                        	Install vlc
```






## Related projects

Check out these related projects: 

- [Packages](https://github.com/cloudposse/packages) - Cloud Posse installer and distribution of native apps
- [Dev Harness](https://github.com/cloudposse/dev) - Cloud Posse Local Development Harness




## References

For additional context, refer to some of these links. 

- [Wikipedia - Test Harness](https://en.wikipedia.org/wiki/Test_harness) - The `build-harness` is similar in concept to a "Test Harness"




## Resources

Resources used to create this project: 

- [Photo](https://unsplash.com/photos/tL92LY152Sk) - Photo by Kai Pilger on Unsplash
- [Gitignore.io](https://gitignore.io) - Defining the `.gitignore`
- [LunaPic](https://www341.lunapic.com/editor/) - Image editor (used to create the avatar)





## Repository

We use [SemVer](http://semver.org/) for versioning. 

- **[Branches][branches]**
- **[Commits][commits]**
- **[Tags][tags]**
- **[Contributors][contributors]**
- **[Graph][graph]**
- **[Charts][charts]**









## Contributors

Thank you so much for making this project possible: 

- [Valter Silva](https://gitlab.com/valter-silva)



## Copyright

Copyright © 2019-2019 [SkaleSys][company]





[logo]: docs/logo.jpeg


[company]: https://skalesys.com
[contact]: https://skalesys.com/contact
[services]: https://skalesys.com/services
[industries]: https://skalesys.com/industries
[training]: https://skalesys.com/training
[insights]: https://skalesys.com/insights
[about]: https://skalesys.com/about
[join]: https://skalesys.com/Join-Our-Team

[roots]: https://gitlab.com/skalesys/roots
[ansible]: https://ansible.com
[terraform]: http://terraform.io
[packer]: https://www.packer.io
[docker]: https://www.docker.com/
[vagrant]: https://www.vagrantup.com/
[kubernetes]: https://kubernetes.io/
[spinnaker]: https://www.spinnaker.io/
[jenkins]: https://jenkins.io/
[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/




[aws]: https://aws.amazon.com/






[branches]: https://gitlab.com/skalesys/terranix/branches
[commits]: https://gitlab.com/skalesys/terranix/commits
[tags]: https://gitlab.com/skalesys/terranix/tags
[contributors]: https://gitlab.com/skalesys/terranix/graphs
[graph]: https://gitlab.com/skalesys/terranix/network
[charts]: https://gitlab.com/skalesys/terranix/charts


